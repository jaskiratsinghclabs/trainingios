//
//  ViewController.swift
//  calculator
//
//  Created by Click Labs 65 on 1/12/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
// currently working for integers
    
    var isTypingNumber = false      //to ensure number is appended to existing
    
    var firstNumber:Double = 0
    
    var secondNumber:Double = 0
    
    var result: Double = 0
    
    var flag = 0
    
    var newFlag = 1
    
    var operation = ""          //copies operation to be performed
    
    var constTouch = false   //ensure operator pressed once
    
        
    
    @IBOutlet weak var numField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func backspace(sender: AnyObject) {
        
        if result == 0.0 {
        
        var str = numField.text
        
       numField.text = str.substringToIndex(str.endIndex.predecessor())
        
        
        }
        
      
        
        
    }
   
    
    @IBAction func numberTapped(sender: AnyObject) {
        
        var number = sender.currentTitle!    //this code sends title to textfield
        
      
        if isTypingNumber {                  //copied into evey number
            
            numField.text = numField.text! + number!
            
        } else {
            
            numField.text = number
            
            isTypingNumber = true
            
        }
        
        constTouch = false
           }
    
    @IBAction func calculationTapped(sender: AnyObject) {
        
        if constTouch == false {
            
        
        
        if flag == 0 {
            
        
        var str = numField.text
        
        isTypingNumber = false      //this code sends changes value of operation
        
        firstNumber =  (str as NSString).doubleValue //stores value of firstnumber
        
        operation = sender.currentTitle!!   // sends current title into the operation thus copied into every operator
        
       numField.text=""
            
            flag++
            
            
    
        }else{ // in order to perform series of calculation
            
            
                result(isTypingNumber)
            
            firstNumber = result
            
           
            
             isTypingNumber = false      //this code sends changes value of operation
            
            
            operation = sender.currentTitle!!   // sends current title into the operation thus copied into every operator
            
            
            //numField.text=""
            
            flag++
            
            
        }
        }else{
        
           println("constant clicking on operator ")
        
        }
    
        constTouch = true
        
        operation = sender.currentTitle!!
    
    }
    
   
    @IBAction func clearButton(sender: AnyObject) {
        
     numField.text=""
        
        firstNumber = 0
        
        secondNumber = 0
        
        flag = 0
        
        result = 0
        
        constTouch = false
    }
    
    
    @IBAction func posNeg(sender: AnyObject) {
        
    }
    
    
    
    @IBAction func result(sender: AnyObject) {
        
        isTypingNumber = false    //this code represent what equal does
        
        
        
        var newstr = numField.text
        
        secondNumber = (newstr as NSString).doubleValue
        
        if operation == "+" {
            
            
            result = firstNumber + secondNumber
            
                numField.text = "\(result)"
                
                } else if operation == "-" {
            
           
            
             result = firstNumber - secondNumber
            
             numField.text = "\(result)"
        }
           
        
        else if operation == "*" {
            
             result = firstNumber * secondNumber
            
             numField.text = "\(result)"
            
            
        }
       
        else if operation == "/" {
            
            if secondNumber.isZero == false
            
            {
                
                var result = firstNumber / secondNumber
               
                numField.text = "\(result)"
                
            }else{
                
                numField.text = "Invalid"
                
                
            }
            
            
            
        }else{
            
            
        }
        
        operation = ""
        
       
        
        
    }
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

